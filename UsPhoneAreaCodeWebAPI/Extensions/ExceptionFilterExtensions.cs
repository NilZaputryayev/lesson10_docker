﻿using AreaCodesAPI.Exceptions;
using System;
using System.Net;


namespace UsPhoneAreaCodeWebAPI.Extensions
{
    public static class ExceptionFilterExtensions
    {
        public static (HttpStatusCode statusCode,int code) ParseException(this Exception exception)
        {
            return exception switch
            {
                NotFoundException _ => (HttpStatusCode.NotFound,0),
                _ => (HttpStatusCode.InternalServerError,-1),
            };
        }
    }
}
