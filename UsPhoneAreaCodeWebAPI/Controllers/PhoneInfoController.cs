﻿using AreaCodesAPI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsPhoneAreaCodeWebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PhoneInfoController : ControllerBase
    {
       
        private readonly ILogger<PhoneInfoController> _logger;

        public PhoneInfoController(ILogger<PhoneInfoController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult> Get(string number)
        {
            AreaCodes areaCodes = new AreaCodes();
            
            var geo = await areaCodes.GetInfo(number);

            if (geo != null)
            {
                return Ok(geo);
            }
            return NotFound();

        }

    }
}
