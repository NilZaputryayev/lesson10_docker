FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app

COPY . ./
RUN dotnet publish UsPhoneAreaCodeWebAPI -c Release -o UsPhoneAreaCodeWebAPI/out

FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
ENV ASPNETCORE_URLS=http://*:5000
COPY --from=build-env /app/UsPhoneAreaCodeWebAPI/out .
ENTRYPOINT ["dotnet", "UsPhoneAreaCodeWebAPI.dll"]

EXPOSE 5000