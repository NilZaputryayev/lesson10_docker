﻿using AngleSharp;
using AngleSharp.Html.Parser;
using AreaCodesAPI.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace AreaCodesAPI
{
    public class AreaCodesHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);
            var content = await response.Content.ReadAsStringAsync();
            string res = processHTML(content);
            response.Content = new StringContent(res);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            return response;
        }

        private string processHTML(string content)
        {
            var context = BrowsingContext.New(Configuration.Default);
            var parser = context.GetService<IHtmlParser>();
            var document = parser.ParseDocument(content);
            var tSelector = "body > table > tbody > tr > td > table:nth-child(2) > tbody > tr > td:nth-child(2) > table:nth-child(3) > tbody > tr > td";
            var TDs = document.QuerySelectorAll(tSelector);
            Type phoneClass = typeof(NumberInfo);
            Type attributeClass = typeof(TableRowAttribute);
            var dictionary = phoneClass.GetProperties()
                                       .Select(x => new
                                       {
                                           key = x.Name,
                                           prop = ((TableRowAttribute)x.GetCustomAttributes(true).FirstOrDefault()).TableRow
                                       })
                                       .ToDictionary(y => y.key, y => y.prop);

            Dictionary<string, string> tdDict = new Dictionary<string, string>();

            for (int i = 0; i < TDs.Length - 1; i += 2)
            {
                if (!string.IsNullOrEmpty(TDs[i].TextContent) && !tdDict.ContainsKey(TDs[i].TextContent.Replace(":", "")))
                    tdDict.Add(TDs[i].TextContent.Replace(":", ""), TDs[i + 1].TextContent);
            }

            if (tdDict.Count == 0) throw new NotFoundException("Wrong USA phone number.");


            var res = dictionary
                .Select(x => new
                {
                    Key = x.Key,
                    value = tdDict[x.Value]
                })
                .ToDictionary(x => x.Key, x => x.value);

            var json = JsonSerializer.Serialize<NumberInfo>(new NumberInfo(res));

            return json;
        }

    }


}






