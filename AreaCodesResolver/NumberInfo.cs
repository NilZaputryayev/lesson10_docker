﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AreaCodesAPI
{
    public class NumberInfo
    {
        [TableRow("NPA/Area Code")]
        public int NPA { get; set; }

        [TableRow("NXX/Prefix")]
        public int NXX { get; set; }

        [TableRow("City")]
        public string City { get; set; }

        [TableRow("State")]
        public string State { get; set; }

        [TableRow("ZIP Code")]
        public int Zip { get; set; }

        [TableRow("NXX Use Type")]
        public string LineType { get; set; }

        [TableRow("Time Zone")]
        public string Timezone { get; set; }

        [TableRow("Latitude")]
        public double Latitude { get; set; }

        [TableRow("Longitude")]
        public double Longtitude { get; set; }

        [TableRow("Carrier/Company")]
        public string Carrier { get; set; }



        [JsonConstructorAttribute]
        public NumberInfo(int nPA, int nXX, string city, string state, int zip, string lineType, string timezone, double latitude, double longtitude, string carrier)
        {
            NPA = nPA;
            NXX = nXX;
            City = city;
            State = state;
            Zip = zip;
            LineType = lineType;
            Timezone = timezone;
            Latitude = latitude;
            Longtitude = longtitude;
            Carrier = carrier;
        }

        public NumberInfo(Dictionary<string, string> fields)
        {
            foreach (var prop in typeof(NumberInfo).GetProperties())
            {
                var tip = prop.PropertyType;
                var tempval = fields[prop.Name];
                object val = null;

                int intRes = 0;

                if (tip == typeof(int)) val = Int32.TryParse(tempval, out intRes) ? intRes : 0;
                else if (tip == typeof(double)) val = Double.Parse(tempval);
                else val = tempval;


                prop.SetValue(this, val);
            }
        }

        public override string ToString()
        {
            return $"City: {City}, State: {State}, Zip: {Zip}, LineType: {LineType}, Carrier: {Carrier}, Latitude: {Latitude}, Longtitude: {Longtitude}";
        }
    }



    

}
