﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using AreaCodesAPI.Exceptions;
using Refit;

namespace AreaCodesAPI
{

    public class AreaCodes
    {
        string BaseUrl = "https://www.area-codes.com/";

        public interface IAreaCodesAPI
        {
            [Get("/exchange/exchange.asp?npa={npa}&nxx={nxx}")]
            Task<NumberInfo> GetGeoInfo([AliasAs("npa")] string npa, [AliasAs("nxx")] string nxx);

        }

        public async Task<NumberInfo> GetInfo(string phoneNumber)
        {
            if (phoneNumber.Length < 10) throw new NotFoundException("Number must be 10 digit");


            var addAreaCodesHandler = new AreaCodesHandler()
            {
                InnerHandler = new HttpClientHandler()
            };
            var httpClient = new HttpClient(addAreaCodesHandler) { BaseAddress = new Uri(BaseUrl) };

            var Api = RestService.For<IAreaCodesAPI>(httpClient);
            string number = phoneNumber.Substring(phoneNumber.Length - 10, 6);
            var res = await Api.GetGeoInfo(number.Substring(0, 3), number.Substring(3, 3));

            return res;
        }


    }





}
