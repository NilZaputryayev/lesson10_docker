﻿using System;

namespace AreaCodesAPI
{
    [AttributeUsage(AttributeTargets.All)]
    public class TableRowAttribute : Attribute
    {
        public string TableRow
        {
            get; private set;
        }

        public TableRowAttribute(string value)
        {
                this.TableRow = value;
        }

    }



    

}
